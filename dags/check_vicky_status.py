import requests
import datetime
import time
from loguru import logger
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.models import Variable
import pytz

args = {
    'owner': 'dm_rushchak',
    'start_date': datetime.datetime(2021, 1, 1),
    'provide_context': True
}

endpoints = {
    'frontend': [
        'https://vicky.tools/auth/login',
        'https://vicky.tools/auth/success',
        'https://vicky.tools/crm/plans',
        'https://vicky.tools/crm/plans',
        'https://vicky.tools/crm/lists',
        'https://vicky.tools/crm/lists/_id',
        'https://vicky.tools/crm/search/person',
        'https://vicky.tools/crm/search/company',
        'https://vicky.tools/lnd/ads',
        'https://vicky.tools/lnd/extension',
        'https://vicky.tools/lnd/hr',
        'https://vicky.tools/lnd/sales',
        'https://vicky.tools/payments/_id',
        'https://vicky.tools/faq'
    ],
    'backend': [
        'https://api.vicky.tools/'
    ],
    'site': [
        'https://vicky.tools/'
    ],
    'vickyFastApi': [
        'http://162.55.244.217:8000/docs#/default/save_img_save_image_post'
    ]

}

def ifttt_notify_app(massage):
    url = 'https://maker.ifttt.com/trigger/push-app/with/key/cK4xOeHmkBBNCRXFIhytNa'
    val = {'value1': massage}
    x = requests.post(url, data=val)

def ifttt_notify_slack(massage):
    url = 'https://maker.ifttt.com/trigger/push-slack/with/key/cK4xOeHmkBBNCRXFIhytNa'
    val = {'value1': massage}
    x = requests.post(url, data=val)


def check_vicky_status(**kwargs):
    text_error = ''
    for url in endpoints['site']:
        url_result = requests.get(url, timeout=10)
        if url_result.status_code != 200:
            text_error += f'Site - {url} статус: {url_result.status_code} в {datetime.datetime.now(pytz.timezone("Europe/Moscow")).strftime("%H:%M:%S")} \n'

    for url in endpoints['backend']:
        try:
            url_result = requests.get(url, timeout=10)
        except:
            url_result = None
        if url_result == None or url_result.text != '{"status":"ok"}':
            text_error += f'Backend - {url} статус: Something wrong в {datetime.datetime.now(pytz.timezone("Europe/Moscow")).strftime("%H:%M:%S")} \n'

    for url in endpoints['frontend']:
        url_result = requests.get(url, timeout=10)
        if url_result.status_code != 200:
            text_error += f'Frontend - {url} статус: {url_result.status_code} в {datetime.datetime.now(pytz.timezone("Europe/Moscow")).strftime("%H:%M:%S")} \n'

    for url in endpoints['vickyFastApi']:
        url_result = requests.get(url, timeout=10)
        if url_result.status_code != 200:
            text_error += f'vickyFastApi - {url} статус: {url_result.status_code} в {datetime.datetime.now(pytz.timezone("Europe/Moscow")).strftime("%H:%M:%S")} \n'

    ti = kwargs['ti']
    ti.xcom_push(key='text_error', value=text_error)


def notify(**kwargs):
    ti = kwargs['ti']
    text_error = ti.xcom_pull(key='text_error', task_ids=['check_vicky_status'])[0]
    if bool(text_error):
        print(type(text_error), text_error)
        try:
            ifttt_notify_app(text_error)
            ifttt_notify_slack(text_error)
        except:
            pass
    else:
        print('All statuses OK')


with DAG('check_vicky_status', description='Отслеживание статусов ендпоинтов vicky.tools', schedule_interval='*/5 * * * *', catchup=False,
         default_args=args) as dag:
    check_vicky_status = PythonOperator(task_id='check_vicky_status', python_callable=check_vicky_status)
    notify = PythonOperator(task_id='notify', python_callable=notify)


    check_vicky_status >> notify

